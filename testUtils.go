package main

import (
	"container/list"
	"github.com/stretchr/testify/suite"
	"log"
	"net"
	"strconv"
	"strings"
	"time"
)

var now = timeZero

const localhost = "127.0.0.1"

type atpTestSuite struct {
	suite.Suite
	timestamp time.Time
}

func (suite *atpTestSuite) handleTestError(err error) {
	if err != nil {
		suite.Errorf(err, "Error occurred")
	}
}

type testUDP struct {
	savedSegments        []*segment
	toDropOnce           []uint32
	timeout              time.Duration
	artificialNow        time.Time
	channelToCommunicate list.List
	sharedBuffer         chan []byte
}

func (manipulator *testUDP) ReadFromUDP(buffer []byte) (int, *net.UDPAddr, error) {
	var buff []byte
	//manipulator.savedSegments  = append(manipulator.savedSegments,c.)
	if manipulator.timeout == 0 {
		buff = <-manipulator.sharedBuffer
		data := buffer[8:len(buff)]
		seg := createSegment(data)
		manipulator.savedSegments = append(manipulator.savedSegments, seg)
		copy(buffer, buff)

		return len(buff), nil, nil
	}
	for {
		select {
		case buff = <-manipulator.sharedBuffer:
			if buff == nil {
				continue
			}
			data := buffer[8:len(buff)]
			seg := createSegment(data)
			manipulator.savedSegments = append(manipulator.savedSegments, seg)
			copy(buffer, buff)
			return len(buff), nil, nil
			//case <-connector.after(timestamp, connector.timeout):
			//	return timeout, manipulator.buffer, 0, nil
		}
	}
}

func (manipulator *testUDP) WriteToUDP(buffer []byte, addr *net.UDPAddr) (int, error) {
	data := buffer[8:]
	seg := createSegment(data)
	manipulator.savedSegments = append(manipulator.savedSegments, seg)

	for i := 0; i < len(manipulator.toDropOnce); i++ {
		if manipulator.toDropOnce[i] == seg.GetSequenceNumber() {
			manipulator.toDropOnce = append(manipulator.toDropOnce[:i], manipulator.toDropOnce[i+1:]...)
			i--
			return len(buffer), nil
		}
	}
	manipulator.sharedBuffer <- buffer
	return len(buffer), nil
}
func (manipulator *testUDP) Close() error {
	return nil
}

func (manipulator *testUDP) pushChannelToCommunicate(channel chan []byte) {
	manipulator.channelToCommunicate.PushBack(channel)
}

func (manipulator *testUDP) DropOnce(sequenceNumber uint32) {
	manipulator.toDropOnce = append(manipulator.toDropOnce, sequenceNumber)
}

//staff not using yet

func repeatDataSizeInc(s int, n int) string {
	str := ""
	for i := 0; i < n; i++ {
		str += strings.Repeat(strconv.Itoa(s+i), getDataChunkSize())
	}
	return str
}

func repeatDataSize(s int, n int) string {
	return strings.Repeat(strconv.Itoa(s), n*getDataChunkSize())
}

/*func init() {
	testErrorChannel = make(chan error, 100)
}*/

func timeNow() time.Time {
	if now != timeZero {
		return now
	} else {
		return time.Now()
	}
}

func reportError(err error) {
	if err != nil {
		log.Println(err)
	}
}
