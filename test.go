package main

import (
	"fmt"
	"log"
	"net"
	"strconv"
	"sync"
)

func main() {
	connection1, err := net.ListenUDP("udp", createUDPAddress("127.0.0.1", 3030))
	if err != nil {
		reportError(err)
	}

	connection2, err := net.ListenUDP("udp", createUDPAddress("127.0.0.1", 3031))
	if err != nil {
		reportError(err)
	}

	psA := SocketListen(&udpConnectorSource{
		local:        connection1,
		errorHandler: func(err error) { log.Printf("Error :%v\n", err) },
	})
	psB := SocketListen(&udpConnectorSource{
		local:        connection2,
		errorHandler: func(err error) { log.Printf("Error :%v\n", err) },
	})
	mutex := sync.WaitGroup{}
	mutex.Add(2)

	go func() {
		conn := psA.ConnectTo(&udpConnectorDestination{
			local:        connection1,
			errorHandler: func(err error) { log.Printf("Error :%v\n", err) },
			remoteAddr:   createUDPAddress("127.0.0.1", 3031),
		})
		conn.Write([]byte("Hello World A"))
		conn.Write([]byte("Hello World C"))
		buffer := make([]byte, 1386)
		conn.Read(buffer)
		fmt.Printf("Socket A Read 1: %v = %v\n", buffer[:13], string(buffer))
		conn.Read(buffer)
		fmt.Printf("Socket A Read 2: %v = %v\n", buffer[:13], string(buffer))
		mutex.Done()
	}()
	go func() {
		conn := psB.Accept()
		conn.Write([]byte("Hello World B"))
		conn.Write([]byte("Hello World D"))
		buffer := make([]byte, 1386)
		conn.Read(buffer)
		fmt.Printf("Socket B Read 1: %v = %v\n", buffer[:13], string(buffer))
		conn.Read(buffer)
		fmt.Printf("Socket B Read 2: %v = %v\n", buffer[:13], string(buffer))
		mutex.Done()
	}()
	mutex.Wait()
}

func createUDPAddress(addressString string, port int) *net.UDPAddr {
	udpAddress, err := net.ResolveUDPAddr("udp4", net.JoinHostPort(addressString, strconv.Itoa(port)))
	reportError(err)
	return udpAddress
}
