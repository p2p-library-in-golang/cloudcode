package main

import "C"
import (
	"fmt"
	"math"
	"math/rand"
	"sync"
	"time"
)

//TODO: fast recovery fehlt (nicht zu verwechseln mit fast retransmit).

//TODO: next_send_time gem. 4.2.1.
//TODO: Prüfen, ob überall kB z.B deliveryrate

//TODO: "Richtigen" wert für delivery rate herausfinden.


const (
	Startup bbrState = iota
	Drain
	ProbeBW
	ProbeRTT
)

const RTpropFilterLen = 10 * time.Second //A constant specifying the length of the RTProp min filter window, RTpropFilterLen is 10 secs.
const BBRHighGain = 2 / math.Ln2         //A constant specifying the minimum gain value that will allow the sending rate to double each round (2/ln(2) ~= 2.89), used in Startup mode for both BBR.pacing_gain and BBR.cwnd_gain.
const BtlBwFilterLen = 10                //A constant specifying the length of the BBR.BtlBw max filter window for BBR.BtlBwFilter, BtlBwFilterLen is 10 packet-timed round trips.
const InitialCwnd float32 = 4 * segmentMtu
const BBRGainCycleLen = 8                       //the number of phases in the BBR ProbeBW gain cycle.
const BBRMinPipeCwnd float32 = 4 * segmentMtu   //The minimal cwnd value BBR tries to target using: 4 packets
const packets_delivered float32 = 1400          //packets_delivered are packets, that were newly acked. --> Here we can set it to 1400, because we can only set 1 packet per ack.
const ProbeRTTDuration = 200 * time.Millisecond //A constant specifying the minimum duration for which ProbeRTT state holds inflight to BBRMinPipeCwnd or fewer packets

var maxDurationTime = time.Duration(time.Unix(1<<63-62135596801, 999999999).Unix())
var cwnd = BBRMinPipeCwnd //The transport sender's congestion window, which limits the amount of data in flight.

type BBR struct {
	delivered            float32
	round_start          bool          //A boolean that BBR sets to true once per packet-timed round trip, on ACKs that advance BBR.round_count.
	round_count          int           //Count of packet-timed round trips.
	BtlBw                float32       //BBR's estimated bottleneck bandwidth available to the transport flow, estimated from the maximum delivery rate sample in a sliding window.
	BtlBwFilter          *BtlBwFilter  //The max filter used to estimate BBR.BtlBw.
	next_round_delivered float32       //packet.delivered value denoting the end of a packet-timed round trip.
	rtprop_expired       bool          //A boolean recording whether the BBR.RTprop has expired and is due for a refresh with an application idle period or a transition into ProbeRTT state.
	rtprop_stamp         time.Time     //The wall clock time at which the current BBR.RTProp sample was obtained.
	RTprop               time.Duration //BBR's estimated two-way round-trip propagation delay of the path, estimated from the windowed minimum recent round-trip delay sample.
	filled_pipe          bool          //A boolean that records whether BBR estimates that it has ever fully utilized its available bandwidth ("filled the pipe").
	full_bw              float32
	full_bw_count        int
	state                bbrState
	pacing_gain          float32 //The dynamic gain factor used to scale BBR.BtlBw to produce BBR.pacing_rate.
	cwnd_gain            float32 //The dynamic gain factor used to scale the estimated BDP to produce a congestion window (cwnd).
	estimated_bdp        float32
	send_quantum         float32 //The maximum size of a data aggregate scheduled and transmitted together.
	target_cwnd          float32
	cycle_stamp          time.Time
	cycle_index          uint32
	probe_rtt_done_stamp time.Time
	probe_rtt_round_done bool
	idle_restart         bool
	prior_cwnd           float32
	pacing_rate          float32 //The current pacing rate for a BBR flow, which controls inter-packet spacing.
	packet_conservation  bool
	prior_inflight       float32
	InLossRecovery       bool
	packets_lost         uint32
	packets_lost_sync    uint32
	mu                   sync.Mutex

	last_time_send		time.Time
}

func newBBR() *BBR {
	return &BBR{
		//delivered: 0,
		//RTprop: time.Duration(0),
		//TODO: initialisation needed here??
		BtlBwFilter: new(BtlBwFilter),
	}
}

func (bbr *BBR) BBRUpdateOnACK(arq *selectiveArq, seg *segment, rs *rateSample, numPacketsLost uint32) {
	bbr.packets_lost = numPacketsLost //Set packets_lost for this ack.
	bbr.BBRUpdateModelAndState(arq, seg, rs)
	bbr.BBRUpdateControlParameters(arq)
	bbr.prior_inflight = float32(arq.waitingForAck.NumOfSegments())
}

func (bbr *BBR) BBRUpdateModelAndState(arq *selectiveArq, seg *segment, rs *rateSample) {
	bbr.BBRUpdateBtlBw(seg, rs)
	bbr.BBRCheckCyclePhase()
	bbr.BBRCheckFullPipe(rs)
	bbr.BBRCheckDrain(arq)
	bbr.BBRUpdateRTprop(seg)
	//bbr.BBRCheckProbeRTT(arq) //vorerst warten mit Aufruf.
}

// if packets_lost is not 0, Thread from getPacketsLost did not read and set it to 0.
func (bbr *BBR) SetPacketsLost(numOfPackets uint32) {
	bbr.mu.Lock()
	defer bbr.mu.Unlock()

	if bbr.packets_lost == 0 {
		bbr.packets_lost = numOfPackets
	} else {
		bbr.packets_lost = bbr.packets_lost + numOfPackets
	}
}

func (bbr *BBR) GetPacketsLost() uint32 {
	bbr.mu.Lock()
	defer bbr.mu.Unlock()
	lost := bbr.packets_lost
	bbr.packets_lost = 0
	return lost
}

type BtlBwFilter struct {
	list   []float32
	maxPos int
	first  int
	last   int
}

func init_windowed_max_filter(filter *BtlBwFilter, value float32) {
	filter.last = 9
	filter.list = make([]float32, BtlBwFilterLen)
}

func update_windowed_max_filter(filter *BtlBwFilter, value float32) float32 {
	var max = filter.list[filter.maxPos]
	var last_pos = filter.last
	filter.list[filter.last] = value
	filter.first = (filter.first + 1) % BtlBwFilterLen
	filter.last = (filter.last + 1) % BtlBwFilterLen

	if value > max {
		filter.maxPos = last_pos
		return filter.list[last_pos]
	}
	if last_pos == filter.maxPos {
		filter.maxPos = findNewMaximum(filter)
	}

	return filter.list[filter.maxPos]
}

func findNewMaximum(filter *BtlBwFilter) int {
	var max float32 = 0
	var maxPos = 0
	for i := 0; i < BtlBwFilterLen; i++ {
		if max < filter.list[i] {
			max = filter.list[i]
			maxPos = i
		}
	}
	return maxPos
}

//Bottleneck Bandwith anhand estimated Bottleneck Bandwith updaten.
func (bbr *BBR) BBRUpdateBtlBw(seg *segment, rs *rateSample) {
	bbr.BBRUpdateRound(seg)
	if rs.delivery_rate >= bbr.BtlBw || !rs.is_app_limited {
		bbr.BtlBw = update_windowed_max_filter(bbr.BtlBwFilter, rs.delivery_rate)
	}
}

func (bbr *BBR) SetBbrDeliveredConcurrent(s *segment){
	bbr.mu.Lock()
	defer bbr.mu.Unlock()
	s.bbrDelivered = bbr.delivered

}

//round start wird false, sobald ein ack nach dem Senden des Sentinentel packets da ist.
func (bbr *BBR) BBRUpdateRound(seg *segment) {
	bbr.delivered++ //float32(len(seg.buffer))  //TODO wenn alles funktioniert aus segmenten bytes machen.
	if seg.bbrDelivered >= bbr.next_round_delivered {
		bbr.next_round_delivered = bbr.delivered
		bbr.round_count++
		bbr.round_start = true
	} else {
		bbr.round_start = false
	}
}

//update RTprop with newest measured rtt
func (bbr *BBR) BBRUpdateRTprop(seg *segment) {
	bbr.rtprop_expired = time.Now().After(bbr.rtprop_stamp.Add(RTpropFilterLen))
	//fmt.Print("seg.rtt", seg.rtt, "\n")
	if seg.rtt > 0 && (seg.rtt <= bbr.RTprop || bbr.rtprop_expired) { //look for greater than 0, because of DivBy0
		bbr.RTprop = seg.rtt
		bbr.rtprop_stamp = time.Now()
	}
}

//bbr.filled_pipe muss nicht implementiert werden, da von BBR selbst schon implementiert.
//Wird geprüft, ob volle pipe erreicht und wenn ja wird bbr.filled_pipe = true.
func (bbr *BBR) BBRCheckFullPipe(rs *rateSample) {
	if bbr.filled_pipe || !bbr.round_start  { //|| rs.is_app_limited
		fmt.Print("rounstart ", bbr.filled_pipe,"\n")

		return
	} // no need to check for a full pipe now
	if bbr.BtlBw >= bbr.full_bw*1.25 { // BBR.BtlBw still growing?
		bbr.full_bw = bbr.BtlBw // record new baseline level
		fmt.Print("notgrou ", bbr.filled_pipe,"\n")

		bbr.full_bw_count = 0
		return
	}
	bbr.full_bw_count++ // another round w/o much growth
	fmt.Print("filledpipe ", bbr.filled_pipe,"\n")
	if bbr.full_bw_count >= 3 {
		bbr.filled_pipe = true
	}

}

//Sobald man bbr.filled_pipe hat werden zusätzliche Packete in die Queue gelegt.
//Die queue muss drained werden, um optimals BBR BtlBW zu erreichen. Dies wird überprüft mit packets_in_flight
//Sobald packets_in_flight gleich (oder kleiner) ist wie das maximal erlaubte (bbr.BBRInflight) wird in ProbeBW gewechselt.
func (bbr *BBR) BBRCheckDrain(arq *selectiveArq) {
	if bbr.state == Startup && bbr.filled_pipe {
		fmt.Print("drAIN\n")
		bbr.BBREnterDrain()
	}
	if bbr.state == Drain && float32(arq.waitingForAck.NumOfSegments()) <= bbr.BBRInflight(1.0) { //TODO
		bbr.BBREnterProbeBW()
	} // we estimate queue is drained
}

func (bbr *BBR) BBREnterDrain() {
	bbr.state = Drain
	bbr.pacing_gain = 1 / BBRHighGain // pace slowly
	bbr.cwnd_gain = BBRHighGain       // maintain cwnd
}

//Hier wird nach der Startphase die meiste Zeit verbracht, wobei man den paicing_gain mal höher und mal tiefer setzt.
func (bbr *BBR) BBREnterProbeBW() {
	fmt.Print("ProbeBW\n")
	bbr.state = ProbeBW
	bbr.pacing_gain = 1
	bbr.cwnd_gain = 2
	bbr.cycle_index = uint32(BBRGainCycleLen - 1 - rand.Intn(7))
	bbr.BBRAdvanceCyclePhase()
}

//The BBR BBR.target_cwnd is the upper bound on the volume of data BBR allows in flight
func (bbr *BBR) BBRInflight(gain float32) float32 {
	if bbr.RTprop == maxDurationTime {
		return InitialCwnd /* no valid RTT samples yet */
	}
	//var quanta = 3 * bbr.send_quantum
	var estimated_bdp = bbr.BtlBw * float32(bbr.RTprop)
	return gain*estimated_bdp //+ quanta
}


func (bbr *BBR) BBRUpdateTargetCwnd() {
	bbr.target_cwnd = bbr.BBRInflight(bbr.cwnd_gain)
}

func (bbr *BBR) BBRAdvanceCyclePhase() {
	bbr.cycle_stamp = time.Now()
	bbr.cycle_index = (bbr.cycle_index + 1) % BBRGainCycleLen
	pacing_gain_cycle := [8]float32{1.25, 0.75, 1, 1, 1, 1, 1, 1}
	fmt.Print("AdvancedCycle",bbr.cycle_index,"----------------------------------\n")
	bbr.pacing_gain = pacing_gain_cycle[bbr.cycle_index]
	fmt.Print("pg: ",bbr.pacing_gain,"\n")
}

//Überprüfe, ob ein RTT abgelaufen ist und somit der Pacing_gain updated werden muss.
//Pacing_gain, which controls how fast packets are sent relative to BBR.BtlBw and is thus key to BBR's ability to learn.
//A pacing_gain > 1 decreases inter-packet time and increases inflight.
//A pacing_gain < 1 has the opposite effect, increasing inter-packet time and generally decreasing inflight.
func (bbr *BBR) BBRCheckCyclePhase() {
	fmt.Print("bbrState: ", bbr.state,"\n" )
	if bbr.state == ProbeBW && bbr.BBRIsNextCyclePhase() {
		bbr.BBRAdvanceCyclePhase()
	}
}

// prior_inflight:  amount of data that was in flight before processing this ACK
func (bbr *BBR) BBRIsNextCyclePhase() bool {
	var is_full_length = (time.Now().Sub(bbr.cycle_stamp)) > bbr.RTprop
	fmt.Print("is_full_length: ", is_full_length)
	if bbr.pacing_gain == 1 {
		return is_full_length
	}
	if bbr.pacing_gain > 1 {
		return is_full_length && (bbr.packets_lost > 0 || bbr.prior_inflight >= bbr.BBRInflight(bbr.pacing_gain)) //TODO
	} else { //  (BBR.pacing_gain < 1)
		return is_full_length || bbr.prior_inflight <= bbr.BBRInflight(1.0)
	}
}

func (bbr *BBR) BBRCheckProbeRTT(arq *selectiveArq) {
	if bbr.state != ProbeRTT && bbr.rtprop_expired && !bbr.idle_restart {
		bbr.BBREnterProbeRTT()
		bbr.BBRSaveCwnd()
		bbr.probe_rtt_done_stamp = timeZero
	}
	if bbr.state == ProbeRTT {
		bbr.BBRHandleProbeRTT(arq)
		bbr.idle_restart = false
	}
}

func (bbr *BBR) BBREnterProbeRTT() {
	bbr.state = ProbeRTT
	bbr.pacing_gain = 1
	bbr.cwnd_gain = 1
}

//Diese Funktion vorerst nicht brauchen bzw. erst, wenn alle anderen korrekt laufen, da dieser Zustand fast nie erreicht wird.
func (bbr *BBR) BBRHandleProbeRTT(arq *selectiveArq) {
	/* Ignore low rate samples during ProbeRTT: */
	//C.app_limited = (BW.delivered + packets_in_flight) ?: 1 TODO??
	if bbr.probe_rtt_done_stamp.Equal(timeZero) && float32(arq.waitingForAck.NumOfSegments()) <= BBRMinPipeCwnd {
		bbr.probe_rtt_done_stamp = time.Now().Add(ProbeRTTDuration)
		bbr.probe_rtt_round_done = false
		bbr.next_round_delivered = bbr.delivered
	} else if !bbr.probe_rtt_done_stamp.Equal(timeZero) {
		if bbr.round_start {
			bbr.probe_rtt_round_done = true
		}
		if bbr.probe_rtt_round_done && time.Now().After(bbr.probe_rtt_done_stamp) {
			bbr.rtprop_stamp = time.Now()
			bbr.BBRRestoreCwnd()
			bbr.BBRExitProbeRTT()
		}
	}
}
func (bbr *BBR) BBRExitProbeRTT() {
	if bbr.filled_pipe {
		bbr.BBREnterProbeBW()
	} else {
		bbr.BBREnterStartup()
	}
}

func (bbr *BBR) BBRSaveCwnd() float32 {
	if !bbr.InLossRecovery && bbr.state != ProbeRTT { //TODO
		return cwnd
	} else {
		return Max(bbr.prior_cwnd, cwnd)
	}
}
func (bbr *BBR) BBRRestoreCwnd() {
	cwnd = Max(cwnd, bbr.prior_cwnd)
}

func (bbr *BBR) BBREnterStartup() {
	bbr.state = Startup
	bbr.pacing_gain = BBRHighGain
	bbr.cwnd_gain = BBRHighGain
}

func (bbr *BBR) BBRUpdateControlParameters(arq *selectiveArq) {
	bbr.BBRSetPacingRate()
	bbr.BBRSetSendQuantum() //vorerst warten mit Aufruf. Bzw. eine konstante Value für send_quantum nehmen.
	bbr.BBRSetCwnd(arq)
}

func (bbr *BBR) BBRSetPacingRateWithGain(pacing_gain float32) {
	var rate = pacing_gain * bbr.BtlBw
	if bbr.filled_pipe || rate > bbr.pacing_rate {
		bbr.BBRSetPacingRateConcurret(rate)
	}
}

func (bbr *BBR) BBRSetPacingRateConcurret(rate float32){
	bbr.mu.Lock()
	defer bbr.mu.Unlock()
	bbr.pacing_rate = rate
}

func (bbr *BBR) BBRGetPacingRateConcurret() float32{
	bbr.mu.Lock()
	defer bbr.mu.Unlock()
	return bbr.pacing_rate
}

//Setzen der neuen Rate anhand neuer pacing_gain.
func (bbr *BBR) BBRSetPacingRate() {
	bbr.BBRSetPacingRateWithGain(bbr.pacing_gain)
}

//Diese FUnktion ist für TSO: Zuerst herausfinden in welchem Bereich unsere pacing_rate liegt und erst dann diese Funktion aufrufen.
func (bbr *BBR) BBRSetSendQuantum() {
	if bbr.pacing_rate < 1.2 {
		bbr.send_quantum = 1 * segmentMtu
	} else if bbr.pacing_rate < 24 {
		bbr.send_quantum = 2 * segmentMtu
	} else {
		bbr.send_quantum = float32(math.Min(float64(bbr.pacing_rate), 64))
	}
}

//Auskommentierte Funktionen vorerst nicht aufrufen, da diese erst bei packet_lost zum zug kommen. D.h. vorher ohne packet loss ausprobieren.
func (bbr *BBR) BBRSetCwnd(arq *selectiveArq) {
	bbr.BBRUpdateTargetCwnd()
	//bbr.BBRModulateCwndForRecovery(arq)
	if true { //!bbr.packet_conservation
		if bbr.filled_pipe {
			cwnd = Min(cwnd+packets_delivered, bbr.target_cwnd)
		} else if cwnd < bbr.target_cwnd || bbr.delivered < InitialCwnd {
			cwnd = cwnd + packets_delivered
 		}

		cwnd = Max(cwnd, BBRMinPipeCwnd)
	}
	bbr.BBRModulateCwndForProbeRTT()
}

//This method is not needed, because we never implemented fast recovery
/*func (bbr *BBR) BBRModulateCwndForRecovery() {
	if bbr.packets_lost > 0 {
		cwnd = Max(cwnd-float32(bbr.packets_lost), 1)
	}
	if bbr.packet_conservation {
		cwnd = Max(cwnd, packets_in_flight+bbr.packets_delivered)
>>>>>>> Stashed changes
	}
}*/

func (bbr *BBR) BBRModulateCwndForProbeRTT() {
	if bbr.state == ProbeRTT {
		cwnd = Min(cwnd, BBRMinPipeCwnd)
	}
}

func (bbr *BBR) BBRInit(arq *selectiveArq) {
	init_windowed_max_filter(bbr.BtlBwFilter, 0)
	if time.Duration(arq.sRtt) != 0 {
		bbr.RTprop = time.Duration(arq.sRtt)
	} else {
		bbr.RTprop = maxDurationTime
	}
	bbr.rtprop_stamp = time.Now()
	bbr.probe_rtt_done_stamp = timeZero
	bbr.probe_rtt_round_done = false
	bbr.packet_conservation = false
	bbr.prior_cwnd = 0
	bbr.idle_restart = false
	bbr.BBRInitRoundCounting()
	bbr.BBRInitFullPipe()
	bbr.BBRInitPacingRate(arq)
	bbr.BBREnterStartup()
}

func (bbr *BBR) BBRInitFullPipe() {
	bbr.filled_pipe = false
	bbr.full_bw = 0
	bbr.full_bw_count = 0
}

func (bbr *BBR) BBRInitRoundCounting() {
	bbr.next_round_delivered = 0
	bbr.round_start = false
	bbr.round_count = 0
}

func (bbr *BBR) BBRInitPacingRate(arq *selectiveArq) { //TODO: look at 4.2.1 of  https://tools.ietf.org/id/draft-cardwell-iccrg-bbr-congestion-control-00.html#bbr-control-parameters
	var nominal_bandwidth = InitialCwnd / 0.001
	bbr.BBRSetPacingRateConcurret(BBRHighGain * float32(nominal_bandwidth))
}

func Max(x, y float32) float32 {
	if x < y {
		return y
	}
	return x
}

func Min(x, y float32) float32 {
	if x > y {
		return y
	}
	return x
}
