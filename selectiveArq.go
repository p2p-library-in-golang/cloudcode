package main

import "C"
import (
	"container/list"
	"encoding/binary"
	"fmt"
	"github.com/coolingglas/ringbufwnd"
	"log"
	"math"
	"sync"
	"time"
)

type (
	statusCode int
	connState  int
)

type bbrState int

const (
	success statusCode = iota
	fail
	ackReceived
	invalidSegment
	windowFull
	timeout
	connectionClosed
)

const initialReceiverWindowSize = uint32(1<<16 - 1)
const rttAlpha, rttBeta = 0.125, 0.25

type segmentQueue struct {
	l *list.List
}

func (queue *segmentQueue) push(seg *segment) {
	queue.l.PushBack(seg)
}

func (queue *segmentQueue) peek() *segment {
	return queue.l.Front().Value.(*segment)
}

func (queue *segmentQueue) pop() *segment {
	return queue.l.Remove(queue.l.Front()).(*segment)
}

func (queue *segmentQueue) size() int {
	return queue.l.Len()
}

type selectiveArq struct {
	connection *Conn

	writeMutex   sync.Mutex
	write        func(s *segment) (statusCode, int, error, *segment)
	errorHandler func(err error)

	// receiver
	nextExpectedSequenceNumber uint32
	segmentBuffer              *ringbufwnd.RingBufferRcv
	receiverWindow             uint32

	// sender
	currentSequenceNumber uint32
	writeQueue            *segmentQueue
	waitingForAck         *ringbufwnd.RingBufferSnd
	sendSynFlag           bool

	// RTT
	rttToMeasure          int
	granularity           float64       // clock granularity
	sRtt                  float64       // smoothed round-trip time
	rttVar                float64       // round-trip time variation
	rto                   time.Duration // retransmission timeout
	bbr                   *BBR
	timesCalledRateSample float32 //For measuring avg
	deliveryRateSum       float32 //For measuring avg
}

func newSelectiveArq(writeAction func(*segment) (statusCode, int, error, *segment), errorHandler func(err error), connection *Conn) *selectiveArq {
	return &selectiveArq{
		connection:            connection,
		write:                 writeAction,
		errorHandler:          func(err error) { log.Printf("TesterrorA: %v\n", err) },
		segmentBuffer:         ringbufwnd.NewRingBufferRcv(initialReceiverWindowSize),
		writeQueue:            &segmentQueue{l: list.New()},
		waitingForAck:         ringbufwnd.NewRingBufferSnd(initialReceiverWindowSize),
		receiverWindow:        initialReceiverWindowSize,
		sendSynFlag:           true,
		rttToMeasure:          5,
		granularity:           float64(100 * time.Millisecond),
		rto:                   1 * time.Second,
		bbr:                   newBBR(),
		timesCalledRateSample: 0,
	}
}

// Wandle gegebenen Buffer in Segment um und bei nichtACK Flag schreibe ein ack
func (arq *selectiveArq) processReceivedSegment(buffer []byte) (statusCode, []*segment) {
	if len(buffer) > 0 {
		seg := createSegment(buffer)
		if seg.isFlaggedAs(flagACK) {
			fmt.Print("ack received")

			arq.handleAck(seg, timeNow())
			return ackReceived, nil
		}
		if seg.GetSequenceNumber() >= arq.nextExpectedSequenceNumber {
			arq.segmentBuffer.Insert(seg)
		}
		//TODO: Fragen, ob Write.ACK nach prozessieren mit arq.segmentBuffer.Remove() aufgerufen werden soll.
		arq.writeAck(seg, timeNow())
		fmt.Print("ack wriittern")
	}
	var segs []*segment

	for {
		seg := arq.segmentBuffer.Remove()
		if seg == nil {
			break
		}
		segCasted := seg.(*segment)
		segs = append(segs, segCasted)
	}
	if len(segs) > 0 {
		arq.nextExpectedSequenceNumber = segs[len(segs)-1].GetSequenceNumber() + 1
		return success, segs
	}
	return invalidSegment, nil
}

func (arq *selectiveArq) handleAck(ack *segment, timestamp time.Time) {
	arq.writeMutex.Lock()
	defer arq.writeMutex.Unlock()
	fmt.Print("handleACK: ", time.Now(), "\n")
	lastInOrder := ack.GetSequenceNumber()       // Until this number we can confirm, that all segment were received by the receiver.
	ackedSequence := bytesToUint32(ack.data[:4]) //acual acked segment

	ackedSeg, _, _ := arq.waitingForAck.Remove(ackedSequence)

	if ackedSeg == nil || ackedSequence < lastInOrder {
		return
	}
	rs := &rateSample{}
	generateRatesample(rs, ackedSeg.(*segment), arq.connection, timestamp)
//	success, ratesample := generateRatesample(rs, ackedSeg.(*segment), arq.connection, timestamp)
	//if(success){
	arq.measureRTT(ackedSeg.(*segment), timestamp)
	//	arq.bbr.BBRUpdateOnACK(arq, ackedSeg.(*segment), ratesample, arq.bbr.GetPacketsLost())
	//}

}

func generateRatesample(rs *rateSample, s *segment, c *Conn, timestamp time.Time) (bool, *rateSample) {
	UpdateRateSample(s, rs, c, timestamp)
	/* Clear app-limited field if bubble is ACKed and gone. */
	//if (C.app_limited and C.delivered > C.app_limited)
	c.app_limited = 0

	defaltTime := time.Time{}
	if rs.prior_time == defaltTime {
		return false, nil /* nothing delivered on this ACK */
	}

	/* Use the longer of the send_elapsed and ack_elapsed */
	rs.interval = maxDuration(rs.send_elapsed, rs.ack_elapsed)
	//c.setRsDeliveredConcurrent(rs.delivered)

	rs.delivered = c.delivered - rs.prior_delivered

	//TODO was soll das heissen? müssen wir hier minrtt berechenen?
	/* Normally we expect interval >= MinRTT.
	 * Note that rate may still be over-estimated when a spuriously
	 * retransmitted skb was first (s)acked because "interval"
	 * is under-estimated (up to an RTT). However, continuously
	 * measuring the delivery rate during loss recovery is crucial
	 * for connections suffer heavy or prolonged losses.
	 */
	if rs.interval < c.findMinRTT() {
		fmt.Println("rs.interval <  c.findMinRTT: ", rs.interval < c.findMinRTT())

		return false, rs /* no reliable sample */
	}

	c.updateMinRtt(rs.interval)

	if c.numberOfCount != 0 {
		c.numberOfCount--
		return false, rs
	}

	if rs.interval != 0 {
		c.arq.timesCalledRateSample++

		timeInNanoSecondsInt := float32(rs.interval)/5 //How long does one packet need to traverse forward and back (RTT)
		timeForAllPackets := timeInNanoSecondsInt * 1000 //How long do all packets need to traverse: Should be timeElapsed.
		packetPairsPerSecond := (1/ timeInNanoSecondsInt) * 1000000000 *  1.75//packetsPairs (=packet+ack) How many packetPairs traverse in 1 Second.
 		c.graph.SetRecord(float64(packetPairsPerSecond))
		rs.delivery_rate = packetPairsPerSecond *22 //We estimate that each packet has 1400 B
		c.arq.deliveryRateSum = c.arq.deliveryRateSum + packetPairsPerSecond
		fmt.Print("timeForAllPackets: ",  timeForAllPackets, "\n")
		fmt.Print("rs.delivered: ", rs.delivered, "\n")
		fmt.Print("timeInNanoSecondsInt",time.Duration(timeInNanoSecondsInt), "\n")
		fmt.Print("pacetsPerSecond", packetPairsPerSecond,"\n")
	}
	return true, rs /* we filled in rs with a rate sample */

}

func UpdateRateSample(s *segment, rs *rateSample, c *Conn, timestamp time.Time) {
	/* Update rs when packet is SACKed or ACKed. */
	t := time.Time{}
	if s.delivered_time == t {
		return /* P already SACKed */
	}

	//Anzahl Packete die gelifert wurden, werden mit conn.delivered gezählt.
	c.delivered++
	c.delivered_time = timestamp

	/* Update info using the newest packet: */
	if s.delivered > rs.prior_delivered {
		rs.prior_delivered = s.delivered
	}

	rs.prior_time = s.delivered_time
	rs.is_app_limited = s.is_app_limited
	rs.send_elapsed = s.sent_time.Sub(s.first_sent_time)
	rs.ack_elapsed = c.delivered_time.Sub(s.delivered_time)

	c.first_sent_time = s.sent_time //evtl. prüfen.

	/* Mark the packet as delivered once it's SACKed to
	* avoid being used again when it's cumulatively acked.
	 */
	s.delivered_time = time.Time{}

}

func maxDuration(a, b time.Duration) time.Duration {
	if a > b {
		return a
	}
	return b
}

func (arq *selectiveArq) measureRTT(seg *segment, timestamp time.Time) {
	//if arq.rttToMeasure <= 0 {
	//	return
	//}

	rtt := float64(timestamp.Sub(seg.timestmp))
	fmt.Println("RTT: ", time.Duration(rtt))
	seg.rtt = timestamp.Sub(seg.timestmp)
	fmt.Print("rtt", rtt, "\n")
	if arq.sRtt == 0 {
		arq.sRtt = rtt
		arq.rttVar = arq.sRtt / 2
		arq.rto = time.Duration(arq.sRtt + math.Max(arq.granularity, 4*arq.rttVar))
	} else {
		arq.rttVar = (1-rttBeta)*arq.rttVar + rttBeta*math.Abs(arq.sRtt-rtt)
		arq.sRtt = (1-rttAlpha)*arq.sRtt + rttAlpha*rtt
		arq.rto = time.Duration(arq.sRtt + math.Max(arq.granularity, 4*arq.rttVar))
	}
	arq.rttToMeasure--
}

func (arq *selectiveArq) writeAck(seg *segment, timestamp time.Time) {
	arq.writeMutex.Lock()
	defer arq.writeMutex.Unlock()
	lastInOrder := arq.nextExpectedSequenceNumber
	ack := createAckSegment(lastInOrder, seg.GetSequenceNumber(), arq.segmentBuffer.Size())
	ack.timestmp = timestamp
	status, _, _, _ := arq.write(ack) //TODO
	ack.updateTimestamp(status, timestamp)
}

func (arq *selectiveArq) queueNewSegments(buffer []byte) {
	arq.writeMutex.Lock()
	defer arq.writeMutex.Unlock()

	var seg *segment
	currentIndex := 0
	for {
		currentIndex, seg = arq.getNextSegmentInBuffer(currentIndex, arq.getAndIncrementCurrentSequenceNumber(), buffer)
		arq.writeQueue.push(seg)
		if currentIndex >= len(buffer) {
			break
		}
	}
}

func (arq *selectiveArq) getAndIncrementCurrentSequenceNumber() uint32 {
	result := arq.currentSequenceNumber
	arq.currentSequenceNumber++
	return result
}

func (arq *selectiveArq) retransmitTimedOutSegments(timestamp time.Time, bbr *BBR) {
	arq.writeMutex.Lock()
	defer arq.writeMutex.Unlock()

	removed := arq.waitingForAck.GetTimedout(timestamp, 3*time.Second)

	if len(removed) != 0 {
		bbr.InLossRecovery = true //synch
		bbr.SetPacketsLost(uint32(len(removed)))
		bbr.prior_cwnd = bbr.BBRSaveCwnd()
		cwnd = 1

		for _, segInterface := range removed {
			seg := segInterface.(*segment)
			seg.addFlag(flagRTO)
			status, _, _, _ := arq.write(seg) //TODO Sinthu
			seg.wasTimedOut = true
			seg.updateTimestamp(status, timestamp)
		}
	} else {
		bbr.InLossRecovery = arq.waitingForAck.IsInRetrasmission()
	}
}

func (arq *selectiveArq) writeQueuedSegments(timestamp time.Time) (statusCode, error) {
	arq.writeMutex.Lock()
	defer arq.writeMutex.Unlock()
	for arq.writeQueue.size() > 0 {
		if arq.waitingForAck.NumOfSegments() >= arq.receiverWindow {
			return windowFull, nil
		}
		seg := arq.writeQueue.peek()
		timeToWait:= 22/arq.bbr.BBRGetPacingRateConcurret() * 1000000000 //* 1000000000 => to Nanoseconds
		nextSendtime := arq.bbr.last_time_send.Add(time.Duration(timeToWait))
		fmt.Println("time.Duration(timeToWait)", time.Duration(timeToWait))
		for {
			if !time.Now().Before(nextSendtime) {
				break
			}
		}
		status, _, err, writtenSegment := arq.write(seg)
		arq.bbr.last_time_send = time.Now()

		// BBR
		c := arq.connection
		if c.arq.waitingForAck.NumOfSegments() < 1 {
			c.first_sent_time = arq.bbr.last_time_send
			c.delivered_time = arq.bbr.last_time_send
		}
		fmt.Println("Getinfligth: ", c.arq.waitingForAck.NumOfSegments())
		seg.first_sent_time = c.first_sent_time
		seg.delivered_time = c.delivered_time
		seg.delivered = c.delivered
		seg.is_app_limited = c.app_limited != 0

		seg.sent_time = arq.bbr.last_time_send

		//for bbr.go
		c.arq.bbr.SetBbrDeliveredConcurrent(seg) //todo prüfen was das macht. Sinthu

		fmt.Print("write: ", writtenSegment.buffer, "\n")
		writtenSegment.updateTimestamp(status, arq.bbr.last_time_send)
		if status != success {
			return status, err
		}
		inserted, err := arq.waitingForAck.InsertSequence(writtenSegment)
		if inserted {
			arq.writeQueue.pop()
		} else if err == nil {
			_, arq.waitingForAck = arq.waitingForAck.Resize(arq.waitingForAck.Capacity() + initialReceiverWindowSize)
			inserted, err = arq.waitingForAck.InsertSequence(writtenSegment)
			arq.writeQueue.pop()
		}
	}
	return success, nil
}

/*func (c *Conn) setRsDeliveredConcurrent(rsd float32){
	c.mutex.Lock()
	defer c.mutex.Unlock()
	c.RSdelivered = rsd
}

func (c *Conn) getRsDeliveredConcurrent() float32{
	c.mutex.Lock()
	defer c.mutex.Unlock()
	return c.RSdelivered
}*/

func (arq *selectiveArq) getNextSegmentInBuffer(currentIndex int, sequenceNum uint32, buffer []byte) (int, *segment) {
	var next = currentIndex + getDataChunkSize()
	var flag byte = 0
	//	if arq.sendSynFlag {
	//		flag |= flagSYN
	//		arq.sendSynFlag = false
	//	}
	if next >= len(buffer) {
		next = len(buffer)
	}
	return next, createFlaggedSegment(sequenceNum, flag, buffer[currentIndex:next]) //hier wird in segmentsafgeteilt
}

func getDataChunkSize() int {
	return segmentMtu - headerLength
}

func (c *Conn) writeToUdpEncryptedOld(segment *segment) (statusCode, int, error, *segment) {
	//simulate connection to another country
	time.Sleep(25*time.Millisecond)

	buf := make([]byte, segmentMtu)
	copy(buf[8:], segment.buffer)
	binary.BigEndian.PutUint64(buf, c.getConnId())
	n := len(segment.buffer)

	status, written, err := c.endpoint.Write(buf[:n+8])
	return status, written, err, segment
}
