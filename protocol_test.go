package main

import (
	"bytes"
	"fmt"
	"github.com/stretchr/testify/suite"
	"log"
	"net"
	"sync"
	"testing"
	"time"
)

var results = make(map[string][]float64)

type PeerSocketTestSuite struct {
	atpTestSuite
	socketA      *PeerSocket
	socketB      *PeerSocket
	socketC      *PeerSocket
	endpoint1    chan []byte
	endpoint2    chan []byte
	endpoint3    chan []byte
	manipulator1 *testUDP
	manipulator2 *testUDP
	manipulator3 *testUDP
}

func (suite *PeerSocketTestSuite) SetupTest() {
	endpoint1, endpoint2, endpoint3 := make(chan []byte, 1000000), make(chan []byte, 1000000), make(chan []byte, 1000000)
	manipulator1 := &testUDP{
		savedSegments: make([]*segment, 0),
		toDropOnce:    make([]uint32, 0),
		sharedBuffer:  endpoint1,
	}

	manipulator2 := &testUDP{
		savedSegments: make([]*segment, 0),
		toDropOnce:    make([]uint32, 0),
		sharedBuffer:  endpoint2,
	}

	manipulator3 := &testUDP{
		savedSegments: make([]*segment, 0),
		toDropOnce:    make([]uint32, 0),
		sharedBuffer:  endpoint3,
	}
	//set endpoints manually
	//Bis jetzt nur ein Endpoint pro socket. Es wären aber auch mehrere endpoints pro conn möglich.
	//Muss synchronisiert werden.
	manipulator1.pushChannelToCommunicate(endpoint3)
	manipulator2.pushChannelToCommunicate(endpoint1)
	manipulator3.pushChannelToCommunicate(endpoint2)
	socketListen1 := &udpConnectorSource{
		local:        manipulator1,
		errorHandler: func(err error) { log.Printf("Error :%v\n", err) },
	}
	socketListen2 := &udpConnectorSource{
		local:        manipulator2,
		errorHandler: func(err error) { log.Printf("Error :%v\n", err) },
	}
	socketListen3 := &udpConnectorSource{
		local:        manipulator3,
		errorHandler: func(err error) { log.Printf("Error :%v\n", err) },
	}

	suite.socketA = SocketListen(socketListen1)
	suite.socketB = SocketListen(socketListen2)
	suite.socketC = SocketListen(socketListen3)
	suite.endpoint1 = endpoint1
	suite.endpoint2 = endpoint2
	suite.endpoint3 = endpoint3

	suite.manipulator1 = manipulator1
	suite.manipulator2 = manipulator2
	suite.manipulator3 = manipulator3
}

func (suite *PeerSocketTestSuite) TearDownTest() {
	suite.handleTestError(suite.socketA.Close())
	suite.handleTestError(suite.socketB.Close())
	suite.handleTestError(suite.socketC.Close())
}

/*
+---------+                +-----------+
| Sender  |                | Receiver  |
+---------+                +-----------+
     |                           |
     | SYN                       |
     |-------------------------->|
     |                           |
     |                   ACK SYN |
     |<--------------------------|
     |                           |
     |           "Hello World B" |
     |<--------------------------|
     |                           |
     | ACK "Hello World B"       |
     |-------------------------->|
     |                           |
     | "Hello World A"           |
     |-------------------------->|
     |                           |
     |       ACK "Hello World A" |
     |<--------------------------|
     |                           |
*/
/*func (suite *PeerSocketTestSuite) TestOneToOneConnectionNormalBehavior() {
	expectedA := []byte("Hello World A")
	expectedB := []byte("Hello World B")
	mutex := sync.WaitGroup{}
	mutex.Add(2)

	manipulator := &segmentManipulator{
		savedSegments: nil,
		toDropOnce:    make([]uint32, 0),
		buffer:        suite.endpoint2,
	}

	go func() {
		conn := suite.socketA.ConnectTo(manipulator)
		suite.NotNil(conn)
		//Channel's are slow in Performance. So wait until SYN Packet is processed.
		time.Sleep(25 * time.Millisecond)
		n, err := conn.Write(expectedA)
		suite.handleTestError(err)
		buffer := make([]byte, 128)
		n, err = conn.Read(buffer)
		suite.Equal(expectedB, buffer[:n])
		suite.handleTestError(err)
		mutex.Done()
	}()
	go func() {
		conn :=suite.socketB.Accept()
		suite.NotNil(conn)
		n, err := conn.Write(expectedB)
		suite.handleTestError(err)
		buffer := make([]byte, 128)
		n, err = conn.Read(buffer)
		suite.Equal(expectedA, buffer[:n])
		suite.handleTestError(err)
		mutex.Done()
	}()
	mutex.Wait()

	//suite.manipulator1.savedSegments is not synchronized. Therfore wait 1 sec. Must sync it programmatically.
	time.Sleep(1000 * time.Millisecond)

	expectedACK0SegmentA := createAckSegment(0,initialReceiverWindowSize)
	expectedSEQSegmentA := createFlaggedSegment(0, flagSEQ, expectedB)
	expectedACK1SegmentA := createAckSegment(1,initialReceiverWindowSize)

	//set expected for socket A receiver
	expectedBufferA := make([]*segment, 0)
	expectedBufferA = append(expectedBufferA, expectedACK0SegmentA)
	expectedBufferA = append(expectedBufferA, expectedSEQSegmentA)
	expectedBufferA = append(expectedBufferA, expectedACK1SegmentA)
	segmentSliceEquals(suite, expectedBufferA,suite.manipulator1.savedSegments)

	//set expected for socket B receiver
	synBuffer := make([]byte, 0)
	expectedSynSegmentB := createFlaggedSegment(0, flagSYN, synBuffer)
	expectedACKSegmentB := createAckSegment(0,initialReceiverWindowSize)
	expectedSEQSegmentB := createFlaggedSegment(1, flagSEQ, expectedA)
	expectedBufferB := make([]*segment, 0)
	expectedBufferB = append(expectedBufferB, expectedSynSegmentB)
	expectedBufferB = append(expectedBufferB, expectedACKSegmentB)
	expectedBufferB = append(expectedBufferB, expectedSEQSegmentB)
	segmentSliceEquals(suite, expectedBufferB,suite.manipulator2.savedSegments)
}*/

/*func (suite *PeerSocketTestSuite) TestOneToOneConnectionLoosingSegment() {
	expectedA := []byte("Hello World A")
	expectedB := []byte("Hello World B")
	mutex := sync.WaitGroup{}
	mutex.Add(2)

	manipulator := &segmentManipulator{
		savedSegments: nil,
		toDropOnce:    make([]uint32, 0),
		buffer:        suite.endpoint2,
	}

	//Drop SYN packet
	manipulator.DropOnce(0)


	go func() {
		conn := suite.socketA.ConnectTo(manipulator)

		suite.NotNil(conn)
		time.Sleep(25 * time.Millisecond)
		n, err := conn.Write(expectedA)
		suite.handleTestError(err)
		buffer := make([]byte, 128)
		n, err = conn.Read(buffer)
		suite.Equal(expectedB, buffer[:n])
		suite.handleTestError(err)
		mutex.Done()
	}()
	go func() {
		conn :=suite.socketB.Accept()
		suite.NotNil(conn)
		n, err := conn.Write(expectedB)
		suite.handleTestError(err)
		buffer := make([]byte, 128)
		n, err = conn.Read(buffer)
		suite.Equal(expectedA, buffer[:n])
		suite.handleTestError(err)
		mutex.Done()
	}()
	mutex.Wait()


	expectedACK0SegmentA := createAckSegment(0,initialReceiverWindowSize)
	expectedSEQSegmentA := createFlaggedSegment(0, flagSEQ, expectedB)
	expectedACK1SegmentA := createAckSegment(1,initialReceiverWindowSize)

	//set expected for socket A receiver
	expectedBufferA := make([]*segment, 0)
	expectedBufferA = append(expectedBufferA, expectedACK0SegmentA)
	expectedBufferA = append(expectedBufferA, expectedSEQSegmentA)
	expectedBufferA = append(expectedBufferA, expectedACK1SegmentA)
	segmentSliceEquals(suite, expectedBufferA,suite.manipulator1.savedSegments)

	//set expected for socket B receiver
	synBuffer := make([]byte, 0)
	expectedSynSegmentB := createFlaggedSegment(0, flagSYN, synBuffer)
	expectedACKSegmentB := createAckSegment(0,initialReceiverWindowSize)
	expectedSEQSegmentB := createFlaggedSegment(1, flagSEQ, expectedA)
	expectedBufferB := make([]*segment, 0)
	expectedBufferB = append(expectedBufferB, expectedSynSegmentB)
	expectedBufferB = append(expectedBufferB, expectedACKSegmentB)
	expectedBufferB = append(expectedBufferB, expectedSEQSegmentB)
	segmentSliceEquals(suite, expectedBufferB,suite.manipulator2.savedSegments)
}*/

func segmentSliceEquals(suite *PeerSocketTestSuite, segmentSliceExpected []*segment, segmentSliceActual []*segment) {
	suite.Equal(len(segmentSliceExpected), len(segmentSliceActual))
	for i, segmentExpected := range segmentSliceExpected {
		suite.Equal(segmentExpected.buffer, segmentSliceActual[i].buffer)
	}
}

/*func (suite *PeerSocketTestSuite) TestNToNConnection() {
	expectedA := []byte("Hello World A")
	expectedB := []byte("Hello World B")
	expectedC := []byte("Hello World C")
	mutex := sync.WaitGroup{}
	mutex.Add(3)

	go func() {
		conn1 := suite.socketA.ConnectTo(&segmentManipulator{
			savedSegments: nil,
			toDropOnce:    make([]uint32, 0),
			buffer:        suite.endpoint2,
		})
		conn2 := suite.socketA.Accept()

		suite.NotNil(conn1)
		suite.NotNil(conn2)

		n, err := conn2.Write(expectedA)
		suite.handleTestError(err)

		buffer := make([]byte, 128)
		n, err = conn1.Read(buffer)
		suite.Equal(expectedB, buffer[:n])
		suite.handleTestError(err)
		mutex.Done()
	}()
	go func() {
		conn1 := suite.socketB.ConnectTo(&segmentManipulator{
			savedSegments: nil,
			toDropOnce:    make([]uint32, 0),
			buffer:        suite.endpoint3,
		})
		conn2 := suite.socketB.Accept()

		suite.NotNil(conn1)
		suite.NotNil(conn2)

		n, err := conn2.Write(expectedB)
		suite.handleTestError(err)

		buffer := make([]byte, 128)
		n, err = conn1.Read(buffer)
		suite.Equal(expectedC, buffer[:n])
		suite.handleTestError(err)
		mutex.Done()
	}()
	go func() {
		conn1 := suite.socketC.ConnectTo(&segmentManipulator{
			savedSegments: nil,
			toDropOnce:    make([]uint32, 0),
			buffer:        suite.endpoint1,
		})
		conn2 := suite.socketC.Accept()
		suite.NotNil(conn1)
		suite.NotNil(conn2)

		n, err := conn2.Write(expectedC)
		suite.handleTestError(err)

		buffer := make([]byte, 128)
		n, err = conn1.Read(buffer)
		suite.Equal(expectedA, buffer[:n])
		suite.handleTestError(err)
		mutex.Done()
	}()
	mutex.Wait()
}*/

/*func (suite *PeerSocketTestSuite) TestOneToOneConnectionWith1000Segments() {
	var timebegin time.Time
	writeNTimes := 20
	toWrite := []byte("Hello World A")
	var expected bytes.Buffer
	mutex := sync.WaitGroup{}
	mutex.Add(2)

	var connAccept *Conn
	var conn *Conn

	manipulator := &segmentManipulator{
		savedSegments: nil,
		toDropOnce:    make([]uint32, 0),
		buffer:        suite.endpoint2,
	}
	timebegin = time.Now()
	//Problematik evtl. weil sehr viele kurze Packete nacheinander gesendet werden?
	go func() {
		conn = suite.socketA.ConnectTo(manipulator)
		suite.NotNil(conn)
		//Wait that first syn packet is prccessed by receiver.
		time.Sleep(25 * time.Millisecond)
		var err error
		for i := 0; i < writeNTimes; i++ {
			//time.Sleep(1 * time.Nanosecond)
			_, err = conn.Write(toWrite)
		}
		suite.handleTestError(err)

		mutex.Done()
	}()
	go func() {
		for i := 0; i < writeNTimes; i++ {
			expected.WriteString(string(toWrite))
		}
		connAccept = suite.socketB.Accept()
		suite.NotNil(connAccept)
		buffer := make([]byte, len(toWrite) * writeNTimes)
		//wait until buffer is filled with sent content.
		for{
			if connAccept.GetReadBufferLength() == len(buffer){
				break
			}
			//fmt.Print("Waiting\n")
		}
		_, err := connAccept.Read(buffer)
		suite.handleTestError(err)
		suite.Equal(expected.Bytes(), buffer)
		mutex.Done()
	}()
	mutex.Wait()




	fmt.Print("timenow: ",timebegin.Sub(time.Now()), "\n")

}*/

func (suite *PeerSocketTestSuite) TestOneToOneConnectionWith200SegmentsAnd10MilliSecondsDelayOnLocalHost() {
	writeNTimes := 1000
	toWrite := []byte("Greating")
	var expected bytes.Buffer
	connection1, err := net.ListenUDP("udp", createUDPAddress("127.0.0.1", 3032))
	if err != nil {
		reportError(err)
	}

	connection2, err := net.ListenUDP("udp", createUDPAddress("127.0.0.1", 3033))
	if err != nil {
		reportError(err)
	}

	psA := SocketListen(&udpConnectorSource{
		local:        connection1,
		errorHandler: func(err error) { log.Printf("Error :%v\n", err) },
	})
	psB := SocketListen(&udpConnectorSource{
		local:        connection2,
		errorHandler: func(err error) { log.Printf("Error :%v\n", err) },
	})
	mutex := sync.WaitGroup{}
	mutex.Add(2)

	var conn *Conn

	var timeBegin time.Time

	go func() {
		conn = psA.ConnectTo(&udpConnectorDestination{
			local:        connection1,
			errorHandler: func(err error) { log.Printf("Error :%v\n", err) },
			remoteAddr:   createUDPAddress("127.0.0.1", 3033),
		})
		conn.timeToWait = 10 * time.Millisecond
		conn.setMinRTTModulo(60)

		var err error
		timeBegin = time.Now()
		for i := 0; i < writeNTimes; i++ {
			time.Sleep(1 * time.Nanosecond)
			_, err = conn.Write(toWrite)
		}
		suite.handleTestError(err)
		mutex.Done()
	}()
	go func() {
		for i := 0; i < writeNTimes; i++ {
			expected.WriteString(string(toWrite))
		}

		conn := psB.Accept()
		conn.timeToWait = 10 * time.Millisecond
		//rangeMinRTT := 0.03 * float32(writeNTimes)
		//conn.minRTT = make([]time.Duration, int(rangeMinRTT))
		conn.setMinRTTModulo(60)

		buffer := make([]byte, len(toWrite)*writeNTimes)
		for {
			if conn.GetReadBufferLength() == len(buffer) {
				break
			}
			//fmt.Print("Waiting\n")
		}
		_, err := conn.Read(buffer)
		suite.handleTestError(err)
		suite.Equal(expected.Bytes(), buffer)
		mutex.Done()
	}()
	mutex.Wait()
	timeElapsed := time.Now().Sub(timeBegin)

	conn.graph.createGraph("1000Seg10msDelay.png", timeElapsed.Seconds(), float64(writeNTimes))
	/*res := make([]float64, 2)
	packetsSendEstimate:= average*timeElapsed.Seconds()
	res[0]= packetsSendEstimate
	res[1]= packetsSendEstimate/float64(writeNTimes)
	results["200SegmentsAnd10MilliSecondsDelay"] =  res*/

}

func filterSiceValuesgreaterOrEqualThan(slice []float64, val float64) {
	i := 0 // output index
	for _, x := range slice {
		if x < val {
			// copy and increment index
			slice[i] = x
			i++
		}
	}
}

/*func (suite *PeerSocketTestSuite) TestOneToOneConnectionWith1000SegmentsAnd30MilliSecondsDelayOnLocalHost() {
	writeNTimes := 200
	toWrite := []byte("Hello World A")
	var expected bytes.Buffer
	errorChannel1 := make(chan error, 100)
	connection1, err := net.ListenUDP("udp", createUDPAddress("127.0.0.1", 3030))
	if err != nil {
		reportError(err)
	}

	errorChannel2 := make(chan error, 100)
	connection2, err := net.ListenUDP("udp", createUDPAddress("127.0.0.1", 3031))
	if err != nil {
		reportError(err)
	}

	psA := SocketListen(&udpConnectorSource{
		local:        connection1,
		timeout:      defaultReadTimeout,
		errorChannel: errorChannel1,
	})
	psB := SocketListen(&udpConnectorSource{
		local:        connection2,
		timeout:      defaultReadTimeout,
		errorChannel: errorChannel2,
	})
	mutex := sync.WaitGroup{}
	mutex.Add(2)

	var conn *Conn

	var timeBegin time.Time

	go func() {
		conn = psA.ConnectTo(&udpConnectorDestination{
			local:        connection1,
			timeout:      defaultReadTimeout,
			errorChannel: errorChannel1,
			remoteAddr:   createUDPAddress("127.0.0.1", 3031),
		})
		conn.timeToWait = 30 * time.Millisecond

		var err error
		timeBegin = time.Now()
		for i := 0; i < writeNTimes; i++ {
			time.Sleep(1 * time.Nanosecond)
			_, err = conn.Write(toWrite)
		}
		suite.handleTestError(err)
		mutex.Done()
	}()
	go func() {
		for i := 0; i < writeNTimes; i++ {
			expected.WriteString(string(toWrite))
		}

		conn := psB.Accept()
		conn.timeToWait = 30 * time.Millisecond

		buffer := make([]byte, len(toWrite)*writeNTimes)
		for {
			if conn.GetReadBufferLength() == len(buffer) {
				break
			}
			//fmt.Print("Waiting\n")
		}
		_, err := conn.Read(buffer)
		suite.handleTestError(err)
		suite.Equal(expected.Bytes(), buffer)
		mutex.Done()
	}()
	mutex.Wait()
	timeElapsed := time.Now().Sub(timeBegin)
	fmt.Print("timeElapsed: ", timeElapsed, "\n")

	/*records := conn.arq.bbr.getbltbwRecords()
	mySlice := make([]float64, len(records))
	for i := float64(0); i < float64(len(records)); i++ {
		mySlice[int(i)] = i
	}

	average := sum(records) / float64(len(mySlice))

	fmt.Print("avgnew:", average, "\n")

	fmt.Print("estimatePacketsSend: ", average*timeElapsed.Seconds(), "\n")
	res := make([]float64, 2)
	packetsSendEstimate:= average*timeElapsed.Seconds()
	res[0]= packetsSendEstimate
	res[1]= packetsSendEstimate/float64(writeNTimes)

	results["200SegmentsAnd30MilliSecondsDelay"] =  res

	graph := chart.Chart{
		XAxis: chart.XAxis{
			TickPosition: chart.TickPositionBetweenTicks,
			ValueFormatter: func(v interface{}) string {
				typed := v.(float64)
				typedDate := chart.TimeFromFloat64(typed)
				return fmt.Sprintf("%d-%d\n%d", typedDate.Month(), typedDate.Day(), typedDate.Year())
			},
		},
		Series: []chart.Series{
			chart.ContinuousSeries{
				XValues: mySlice,
				YValues: records,
			},

		},
	}

	f, _ := os.Create("output2.png")
	defer f.Close()
	graph.Render(chart.PNG, f)

}

func (suite *PeerSocketTestSuite) TestOneToOneConnectionWith100SegmentsAnd80MilliSecondsDelayOnLocalHost() {
	writeNTimes := 100
	toWrite := []byte("Hello World A")
	var expected bytes.Buffer
	errorChannel1 := make(chan error, 100)
	connection1, err := net.ListenUDP("udp", createUDPAddress("127.0.0.1", 3034))
	if err != nil {
		reportError(err)
	}

	errorChannel2 := make(chan error, 100)
	connection2, err := net.ListenUDP("udp", createUDPAddress("127.0.0.1", 3035))
	if err != nil {
		reportError(err)
	}

	psA := SocketListen(&udpConnectorSource{
		local:        connection1,
		timeout:      defaultReadTimeout,
		errorChannel: errorChannel1,
	})
	psB := SocketListen(&udpConnectorSource{
		local:        connection2,
		timeout:      defaultReadTimeout,
		errorChannel: errorChannel2,
	})
	mutex := sync.WaitGroup{}
	mutex.Add(2)

	var conn *Conn

	var timeBegin time.Time

	go func() {
		conn = psA.ConnectTo(&udpConnectorDestination{
			local:        connection1,
			timeout:      defaultReadTimeout,
			errorChannel: errorChannel1,
			remoteAddr:   createUDPAddress("127.0.0.1", 3035),
		})
		conn.timeToWait = 80 * time.Millisecond

		var err error
		timeBegin = time.Now()
		for i := 0; i < writeNTimes; i++ {
			time.Sleep(1 * time.Nanosecond)
			_, err = conn.Write(toWrite)
		}
		suite.handleTestError(err)
		mutex.Done()
	}()
	go func() {
		for i := 0; i < writeNTimes; i++ {
			expected.WriteString(string(toWrite))
		}

		conn := psB.Accept()
		conn.timeToWait = 80 * time.Millisecond

		buffer := make([]byte, len(toWrite)*writeNTimes)
		for {
			if conn.GetReadBufferLength() == len(buffer) {
				break
			}
			//fmt.Print("Waiting\n")
		}
		_, err := conn.Read(buffer)
		suite.handleTestError(err)
		suite.Equal(expected.Bytes(), buffer)
		mutex.Done()
	}()
	mutex.Wait()
	timeElapsed := time.Now().Sub(timeBegin)
	fmt.Print("timeElapsed: ", timeElapsed, "\n")

	/*records := conn.arq.bbr.getbltbwRecords()
	mySlice := make([]float64, len(records))
	for i := float64(0); i < float64(len(records)); i++ {
		mySlice[int(i)] = i
	}

	average := sum(records) / float64(len(mySlice))

	res := make([]float64, 2)
	packetsSendEstimate:= average*timeElapsed.Seconds()
	res[0]= packetsSendEstimate
	res[1]= packetsSendEstimate/float64(writeNTimes)
	results["100SegmentsAnd80MilliSecondsDelay"] =  res

}*/

func TestSocketConnection(t *testing.T) {
	suite.Run(t, new(PeerSocketTestSuite))
	for key, element := range results {
		fmt.Println("Key:", key, "=>", "Element:", element)
	}
}
