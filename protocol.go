package main

import "C"
import (
	"bytes"
	"container/list"
	"crypto/rand"
	"encoding/binary"
	"fmt"
	"io"
	"log"
	"net"
	"sync"
	"time"
)

const (
	retryTimeout       = 10 * time.Millisecond
	defaultReadTimeout = 10 * time.Millisecond
	defaultMTU         = 1400
	headerLength       = 6
)

const (
	timeoutErrorString          = "i/o timeout"
	connectionClosedErrorString = "use of closed network connection"
)

var timeZero = time.Time{}

type connectorWrite interface {
	io.Closer
	Write([]byte) (statusCode, int, error)
	GetSocket() udp
}

type connectorRead interface {
	io.Closer
	Read([]byte) (statusCode, *net.UDPAddr, int, error)
	GetSocket() udp
}

type PeerSocket struct {
	sourceConnector connectorRead
	errorHandler    func(err error)
	connectionID    uint64
	multiplex       map[uint64]*Conn
	newConnNotifier *sync.Cond
	newConns        *list.List
}

func SocketListen(sourceConnector connectorRead) *PeerSocket {
	peer := &PeerSocket{
		sourceConnector: sourceConnector,
		errorHandler:    func(err error) { log.Printf("Error :%v\n", err) },
		connectionID:    0,
		multiplex:       make(map[uint64]*Conn), //Wird erst bei Connecto zugewiesen.
		newConnNotifier: sync.NewCond(&sync.Mutex{}),
		newConns:        list.New(),
	}
	go peer.write()
	go peer.read()
	return peer
}

//TODO evtl. Typecheck ob gegenüber den gleichen Connector hat?
func (socket *PeerSocket) ConnectTo(connector connectorWrite) *Conn {
	conn := newConn(newConnId(), connector)
	conn.arq = newSelectiveArq(conn.writeToUdpEncryptedOld, socket.errorHandler, conn)
	conn.arq.bbr.BBRInit(conn.arq)
	socket.multiplex[conn.getConnId()] = conn
	//createFlagged segment for syn
	buffer := make([]byte, 0)
	seg := createFlaggedSegment(conn.arq.getAndIncrementCurrentSequenceNumber(), flagSYN, buffer)
	conn.arq.writeQueue.push(seg)
	return conn
}

func (socket *PeerSocket) Accept() *Conn {
	socket.newConnNotifier.L.Lock()
	for socket.newConns.Len() == 0 {
		socket.newConnNotifier.Wait()
	}

	conn := socket.newConns.Front().Value.(*Conn)
	socket.newConnNotifier.L.Unlock()
	socket.multiplex[conn.getConnId()] = conn
	return conn

}

//PeerSocket ConnectTo/Accept() helper
func newConnId() uint64 {
	b := make([]byte, 8)
	_, _ = rand.Read(b)
	return binary.BigEndian.Uint64(b)
}

func (c *Conn) getConnId() uint64 {
	return c.connId
}

func (socket *PeerSocket) write() {
	for {
		for _, c := range socket.multiplex {
			c.arq.retransmitTimedOutSegments(timeNow(), c.arq.bbr)
			_, err := c.arq.writeQueuedSegments(timeNow())
			if err != nil {
				socket.errorHandler(err)
			}

		}
		time.Sleep(retryTimeout)
	}
}

func (socket *PeerSocket) read() {
	for {
		buffer := make([]byte, segmentMtu)
		status, addr, n, err := socket.sourceConnector.Read(buffer) //Hier wird auf dem UDP socket kommuniziert.
		if n != 0 {
			fmt.Print("readloop: ", buffer[:n], "\n")
		}
		if status != success && err == nil {
			continue
		}
		if err != nil {
			socket.errorHandler(err)
		}

		connId := binary.BigEndian.Uint64(buffer[:8])
		data := buffer[8:n] // von incl. bis excl.
		if c, ok := socket.multiplex[connId]; ok {
			status, segs := c.arq.processReceivedSegment(data)
			if status == success {
				c.mutex.Lock()
				for _, seg := range segs {
					c.readBuffer.Write(seg.data) //Schreiben in Read buf von pee
				}
				c.mutex.Unlock()
				if len(c.readNotifier) == 0 {
					c.readNotifier <- true
				}
			}
		} else {
			if seg := createSegment(data); seg.isFlaggedAs(flagSYN) {
				socket.InitializeConnectionUDP(connId, addr, data)
			}
		}
	}
}

//PeerSocket read() helper
func (socket *PeerSocket) InitializeConnectionUDP(connId uint64, remoteAddr *net.UDPAddr, data []byte) {
	conn := newConn(connId, &udpConnectorDestination{
		local:        socket.sourceConnector.GetSocket(),
		errorHandler: func(err error) { log.Printf("Error init: %v\n", err) },
		remoteAddr:   remoteAddr,
	})
	socket.SetupConnection(conn, data)
}

func (socket *PeerSocket) SetupConnection(conn *Conn, data []byte) {
	conn.arq = newSelectiveArq(conn.writeToUdpEncryptedOld, socket.errorHandler, conn)
	conn.arq.bbr.BBRInit(conn.arq)
	socket.newConnNotifier.L.Lock()
	socket.newConns.PushBack(conn)
	socket.newConnNotifier.L.Unlock()
	socket.newConnNotifier.Signal()
	c := conn
	//segs, vermutlich z.b. wenn 3 verloren aber 4, 5 ankommen, dann werden nach Ankunft von 3: 3, 4, 5 in segs geladen.
	status, segs := c.arq.processReceivedSegment(data)

	//TODO, da SYN Packet, müsste dieser Schritt nicht gemacht werden.
	if status == success {
		c.mutex.Lock()
		for _, seg := range segs {
			if !seg.isFlaggedAs(flagSYN) {
				c.readBuffer.Write(seg.data) //Schreiben in Read buf von peer
			}
		}
		c.mutex.Unlock()
		if len(c.readNotifier) == 0 {
			c.readNotifier <- true
		}
	}
}

// Close closes the underlying two-way connection interface, preventing all
// future calls to PeerSocket.Write and PeerSocket.Read from having any effect
func (socket *PeerSocket) Close() error {
	return socket.sourceConnector.Close()
}

type Conn struct {
	endpoint connectorWrite
	arq      *selectiveArq
	state    connState
	connId   uint64

	readBuffer   bytes.Buffer
	readNotifier chan bool
	mutex        sync.Mutex
	timeout      time.Duration

	//BBR
	delivered            float32   //The total amount of data (tracked in packets) delivered so far over the lifetime of the transport connection.
	delivered_time       time.Time //The wall clock time when C.delivered was last
	first_sent_time      time.Time
	app_limited          float32 // The index of the last transmitted packet marked as application-limited, or 0 if the connection is not currently application-limited.
	write_seq            float32 //The data sequence number one higher than that of the last octet queued for transmission in the transport layer write buffer.
	pending_transmission float32 //The number of bytes queued for transmission on the sending host at layers lower than the transport layer (i.e. network layer, traffic shaping layer, network device layer).
	lost_out             float32 //The number of packets in the current outstanding window that are marked as lost
	retrans_out          float32 //The number of packets in the current outstanding window that are being retransmitted.
	pipe                 float32 //The sender's estimate of the number of packets outstanding in the network; i.e. the number of packets in the current outstanding window that are being transmitted or retransmitted and have not been SACKed or marked lost (e.g. "pipe" from [RFC6675]).

	timeToWait time.Duration //For simulating a delay

	minRTT       []time.Duration //For calculting minrtt
	minRTTIndex  int             //For calculting minrtt
	minRTTModulo int             //For calculting minrtt

	graph *Graph

	numberOfCount int //temp
}

func newConn(connId uint64, endpoint connectorWrite) *Conn {
	return &Conn{
		endpoint:     endpoint,
		arq:          nil,
		state:        0,
		connId:       connId,
		readBuffer:   bytes.Buffer{},
		readNotifier: make(chan bool, 1),
		mutex:        sync.Mutex{},
		timeout:      0,
		minRTT:       make([]time.Duration, 10),
		graph: &Graph{
			mu: sync.Mutex{},
		},
		minRTTModulo:  10,
		numberOfCount: 10,
	}
}

func (c *Conn) setMinRTTModulo(num int) {
	c.minRTTModulo = num
	c.minRTT = make([]time.Duration, num)
}

func (c *Conn) updateMinRtt(duration time.Duration) {
	c.minRTT[c.minRTTIndex] = duration
	c.minRTTIndex = (c.minRTTIndex + 1) % c.minRTTModulo
	fmt.Println("c.minRTT: ", c.minRTT)
}

func (c *Conn) findMinRTT() time.Duration {
	min := c.minRTT[0]
	for _, v := range c.minRTT {
		if v < min {
			min = v
		}
	}
	return min
}

func (c *Conn) Write(buffer []byte) (int, error) {
	c.arq.queueNewSegments(buffer)
	return len(buffer), nil
}

func (c *Conn) GetReadBufferLength() int {
	c.mutex.Lock()
	defer c.mutex.Unlock()
	return c.readBuffer.Len()
}

func (c *Conn) Read(buffer []byte) (int, error) {
	c.mutex.Lock()
	for c.readBuffer.Len() == 0 {
		c.mutex.Unlock()
		if c.timeout > 0 {
			select {
			case <-c.readNotifier:
				c.mutex.Lock()
			case <-time.After(c.timeout):
				return 0, nil
			}
		} else {
			select {
			case <-c.readNotifier:
				c.mutex.Lock()
			}
		}
	}
	n, err := c.readBuffer.Read(buffer)
	c.mutex.Unlock()
	return n, err
}

//*********************************************************************************
//                            UDP implementation
//*********************************************************************************

//udp can be -> net.ListenUDP("udp", createUDPAddress("127.0.0.1", 3032))
type udp interface {
	ReadFromUDP(b []byte) (int, *net.UDPAddr, error)
	WriteToUDP(b []byte, addr *net.UDPAddr) (int, error)
	Close() error
}

type udpConnectorDestination struct {
	local        udp
	errorHandler func(err error)
	remoteAddr   *net.UDPAddr
}

func (u udpConnectorDestination) Write(buffer []byte) (statusCode, int, error) {
	n, err := u.local.WriteToUDP(buffer, u.remoteAddr)
	if err != nil {
		return fail, n, err
	}
	return success, n, err
}

func (u udpConnectorDestination) GetSocket() udp {
	return u.local
}

func (u udpConnectorDestination) Close() error {
	return u.local.Close()
}

type udpConnectorSource struct {
	local        udp
	errorHandler func(err error)
}

func (u udpConnectorSource) Read(buffer []byte) (statusCode, *net.UDPAddr, int, error) {
	n, addr, err := u.local.ReadFromUDP(buffer)
	if err != nil {
		switch err.(type) {
		case *net.OpError:
			if err.(*net.OpError).Err.Error() == connectionClosedErrorString {
				return connectionClosed, nil, 0, nil
			}
		}
		return fail, nil, n, err
	}
	return success, addr, n, err
}

func (u udpConnectorSource) GetSocket() udp {
	return u.local
}

func (u udpConnectorSource) Close() error {
	return u.local.Close()
}
