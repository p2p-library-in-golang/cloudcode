# P2P Library in Golang

[![pipeline status](https://gitlab.com/p2p-library-in-golang/code/badges/master/pipeline.svg)](https://gitlab.com/p2p-library-in-golang/code/-/commits/master)
[![coverage report](https://gitlab.com/p2p-library-in-golang/code/badges/master/coverage.svg)](https://gitlab.com/p2p-library-in-golang/code/-/commits/master)
[![pipeline status](https://gitlab.com/p2p-library-in-golang/code/badges/1-test/pipeline.svg)](https://gitlab.com/p2p-library-in-golang/code/-/commits/1-test)
[![coverage report](https://gitlab.com/p2p-library-in-golang/code/badges/1-test/coverage.svg)](https://gitlab.com/p2p-library-in-golang/code/-/commits/1-test)

## Specification

### Data Segment Header
| Data Offset (1B) | Flags (1B) | Sequence Number (4B) | 
| ---------------- | ---------- | -------------------- |


```
Data Offset:
    Byte designating the start of payload data
Flags:
    Single byte for flag bits
Sequence Number:
    32-bit sequence number designating a segments order
```

### ACK Segment
| Data Offset (1B) | Flags (1B) | Sequence Number (4B) | Window Size (3B) |
| ---------------- | ---------- | -------------------- | ---------------- |

```
Data Offset:
    Byte designating the start of payload data
Flags:
    Single byte for flag bits
Sequence Number:
    32-bit sequence number designating a segments order. For ACKS, this is always the last in-order number
Window Size:
    TODO: Communicates the receiver Window size.
```
