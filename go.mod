module SimpleARQ

go 1.16

require (
	github.com/coolingglas/ringbufwnd v0.0.0-20210501181838-340e9b8d81ee
	github.com/stretchr/testify v1.7.0
	github.com/wcharczuk/go-chart/v2 v2.1.0
)
