package main

import (
	"fmt"
	"github.com/wcharczuk/go-chart/v2"
	"os"
	"sync"
)

type Graph struct{
	records 	[]float64
	mu			sync.Mutex
}



func (g *Graph) createGraph(graphName string, elapsedTime float64, numOfSentPackets float64)   {



	xValues := make([]float64, len(g.records))
	for i := float64(0); i < float64(len(g.records)); i++ {
		xValues[int(i)] = i
	}

	average := sum(g.records) / float64(len(g.records))
	packetsSendEstimate:= average*elapsedTime
	ratio := packetsSendEstimate/numOfSentPackets
	graph := chart.Chart{
		Series: []chart.Series{
			chart.ContinuousSeries{
				Name:	"average: " + fmt.Sprintf("%.2f", average)+ " | "+ "packetsSendEstimate " +
					fmt.Sprintf("%.2f", packetsSendEstimate) + " | "+ " Estimate/Actual: "+ fmt.Sprintf("%.2f", ratio),
				XValues: xValues,
				YValues: g.records,
			},
		},
	}

	graph.Elements = []chart.Renderable{
		chart.Legend(&graph),
	}

	f, _ := os.Create(graphName) //z.B output.png
	defer f.Close()
	graph.Render(chart.PNG, f)

	fmt.Println("len(g.records): ",len(g.records) )

}

func (g *Graph) calculateAVG() float64{
	return sum(g.records) / float64(len(g.records))
}

func (g *Graph) calculateProportionActualToEstimate(elapsedTime float64) float64{
	return sum(g.records) / float64(len(g.records))
}



func (g *Graph) SetRecord(record float64){
	g.mu.Lock()
	defer g.mu.Unlock()
		g.records = append(g.records, record)
}


func sum(array []float64) float64 {
	result := float64(0)
	for _, v := range array {
		result += v
	}
	return result
}